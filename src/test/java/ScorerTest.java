import domain.DataCenter;
import domain.DataCenterConfig;
import domain.Row;
import domain.Server;
import domain.Slot;
import org.junit.jupiter.api.Test;
import processor.Scorer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ScorerTest {

    @Test
    void testScoreMethod() {
        //initialize mock DataCenter
        DataCenter dataCenter = new DataCenter();
        DataCenterConfig dataCenterConfig = new DataCenterConfig(2, 5, 1, 2, 5);
        dataCenter.setConfig(dataCenterConfig);

        Row row0 = new Row(0);
        for (int i = 0; i < 5; i++) {
            row0.addSlot(new Slot());
        }
        row0.getSlots().get(0).setAvailable(false);
        dataCenter.addRow(row0);

        Row row1 = new Row(1);
        for (int i = 0; i < 5; i++) {
            row1.addSlot(new Slot());
        }
        dataCenter.addRow(row1);

        List<Server> servers = new ArrayList<>();

        servers.add(new Server(0, 10, 3));
        servers.add(new Server(1, 10, 3));
        servers.add(new Server(2, 5, 2));
        servers.add(new Server(3, 5, 1));
        servers.add(new Server(4, 1, 1));

        servers.get(0).setPoolId(0);
        servers.get(0).setAllocated(true);

        servers.get(1).setPoolId(1);
        servers.get(1).setAllocated(true);

        servers.get(2).setPoolId(0);
        servers.get(2).setAllocated(true);

        servers.get(3).setPoolId(1);
        servers.get(4).setAllocated(true);

        servers.get(4).setAllocated(false);

        dataCenter.setServers(servers);

        //allocate servers manually
        for (int i = 1; i < 4; i++) {
            dataCenter.getRows().get(0).getSlots().get(i).setAvailable(false);
            dataCenter.getRows().get(0).getSlots().get(i).setServerId(0);
        }

        dataCenter.getRows().get(0).getSlots().get(4).setAvailable(false);
        dataCenter.getRows().get(0).getSlots().get(4).setServerId(3);

        for (int i = 0; i < 3; i++) {
            dataCenter.getRows().get(1).getSlots().get(i).setAvailable(false);
            dataCenter.getRows().get(1).getSlots().get(i).setServerId(1);
        }
        for (int i = 3; i < 5; i++) {
            dataCenter.getRows().get(1).getSlots().get(i).setAvailable(false);
            dataCenter.getRows().get(1).getSlots().get(i).setServerId(2);
        }

        int score = Scorer.score(dataCenter);
        assertEquals(5, score);
    }

    @Test
    void  testScoreMethod2(){

        //initialize mock DataCenter
        DataCenter dataCenter = new DataCenter();
        DataCenterConfig dataCenterConfig = new DataCenterConfig(3, 6, 0, 3, 9);
        dataCenter.setConfig(dataCenterConfig);

        for (int i = 0; i < dataCenterConfig.getNumberOfRows(); i++){
            Row row = new Row(i);
            for (int j = 0; j < dataCenterConfig.getNumberOfSlots(); j++){
                row.addSlot(new Slot());
            }
            dataCenter.addRow(row);
        }

        System.out.println(dataCenter);

        List<Server> servers = new ArrayList<>();

        servers.add(new Server(0, 10, 3));
        servers.add(new Server(1, 14, 3));
        servers.add(new Server(2, 7, 3));
        servers.add(new Server(3, 8, 2));
        servers.add(new Server(4, 5, 2));
        servers.add(new Server(5, 3, 2));
        servers.add(new Server(6, 7, 1));
        servers.add(new Server(7, 4, 1));
        servers.add(new Server(8, 5, 1));

        //servers of size 3
        servers.get(0).setPoolId(0);
        servers.get(0).setAllocated(true);

        servers.get(1).setPoolId(2);
        servers.get(1).setAllocated(true);

        servers.get(2).setPoolId(1);
        servers.get(2).setAllocated(true);

        //servers of size 2
        servers.get(3).setPoolId(1);
        servers.get(3).setAllocated(true);

        servers.get(4).setPoolId(0);
        servers.get(4).setAllocated(true);

        servers.get(5).setPoolId(2);
        servers.get(5).setAllocated(true);

        //servers of size 1
        servers.get(6).setPoolId(2);
        servers.get(6).setAllocated(true);

        servers.get(7).setPoolId(1);
        servers.get(7).setAllocated(true);

        servers.get(8).setPoolId(0);
        servers.get(8).setAllocated(true);

        dataCenter.setServers(servers);


        //manually allocate servers
        for (int i = 0; i < 3; i++){
            dataCenter.getRows().get(0).getSlots().get(i).setServerId(0);
            dataCenter.getRows().get(1).getSlots().get(i).setServerId(1);
            dataCenter.getRows().get(2).getSlots().get(i).setServerId(2);
        }

        for (int i = 3; i < 5; i++ ){
            dataCenter.getRows().get(0).getSlots().get(i).setServerId(3);
            dataCenter.getRows().get(1).getSlots().get(i).setServerId(4);
            dataCenter.getRows().get(2).getSlots().get(i).setServerId(5);
        }

        dataCenter.getRows().get(0).getSlots().get(5).setServerId(6);
        dataCenter.getRows().get(1).getSlots().get(5).setServerId(7);
        dataCenter.getRows().get(2).getSlots().get(5).setServerId(8);

        int score = Scorer.score(dataCenter);
        assertEquals(10, score);
    }

}
