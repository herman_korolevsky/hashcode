import domain.DataCenter;
import java.io.IOException;
import java.util.List;
import processor.Allocator;
import processor.FileReader;
import processor.InputProcessor;
import processor.Scorer;

public class Main {

    public static void main(String[] args) {
        FileReader reader = new FileReader();
        InputProcessor inputProcessor = new InputProcessor();
        Allocator allocator = new Allocator();
        List<String> inputLines;

        try {
            inputLines = reader.readFile("data/dc.in");
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        DataCenter dataCenter = inputProcessor.processLines(inputLines);
        allocator.allocate(dataCenter);
        System.out.println("\nFinal score is " + Scorer.score(dataCenter));
    }
}
