package processor;

import domain.DataCenter;
import domain.Row;
import domain.Server;
import domain.Slot;

import java.util.HashSet;
import java.util.Set;

public class Scorer {

    public static int score(DataCenter dataCenter) {

        int numOfPools = dataCenter.getConfig().getNumberOfPools();
        int numOfRows = dataCenter.getConfig().getNumberOfRows();

        int lowestGuaranteedCapacity = Integer.MAX_VALUE;

        for (int i = 0; i < numOfPools; i++) {
            int guaranteedPoolCapacity = Integer.MAX_VALUE;
            for (int j = 0; j < numOfRows; j++) {
                int poolCapacity = 0;
                //j - row to disable
                for (int k = 0; k < numOfRows; k++) {
                    //don't calculate for disabled row
                    if (k == j) {
                        continue;
                    }

                    poolCapacity += getRowCapacityForPool(i, dataCenter.getRows().get(k), dataCenter);

                }
                if (poolCapacity < guaranteedPoolCapacity) {
                    guaranteedPoolCapacity = poolCapacity;
                }
            }
            if (guaranteedPoolCapacity < lowestGuaranteedCapacity) {
                lowestGuaranteedCapacity = guaranteedPoolCapacity;
            }
        }

        return lowestGuaranteedCapacity;
    }


    private static int getRowCapacityForPool(int poolNum, Row row, DataCenter dataCenter) {
        int capacity = 0;
        //set of unique server id's from pool poolnNum in row
        Set<Integer> serverIds = new HashSet<>();
        for (Slot slot : row.getSlots()) {
            if (slot.isAvailable()) {
                serverIds.add(slot.getServerId());
            }
        }

        for (int serverId : serverIds) {
            Server server = dataCenter.getServerById(serverId);
            if (server.getPoolId() == poolNum) {
                capacity += server.getCapacity();
            }
        }

        return capacity;
    }

}
