package processor;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

public class FileReader {

    public List<String> readFile(final String fileName) throws IOException {
        ClassLoader classLoader = FileReader.class.getClassLoader();
        String result = IOUtils.toString(classLoader.getResourceAsStream(fileName));
        System.out.println(result);
        return Arrays.asList(StringUtils.split(result, '\n'));
    }
}
