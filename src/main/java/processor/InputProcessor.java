package processor;

import domain.DataCenter;
import domain.DataCenterConfig;
import domain.Row;
import domain.Server;
import domain.Slot;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class InputProcessor {

    private static final String SPACE = " ";

    public DataCenter processLines(List<String> inputLines) {
        DataCenterConfig config = readConfig(inputLines.get(0));
        DataCenter dataCenter = createDataCenter(config);
        makeSlotsUnavailable(inputLines, config.getNumberOfUnavailableSlots(), dataCenter);
        List<Server> servers = readServersConfig(inputLines, config.getNumberOfServers());
        dataCenter.setServers(servers);
        return dataCenter;
    }

    private void makeSlotsUnavailable(final List<String> inputLines, final int numberOfUnavailableSlots, final DataCenter dataCenter) {
        for (int i = 0; i < numberOfUnavailableSlots; i++) {
            String[] coordinates = inputLines.get(i + 1).split(SPACE);
            int row = Integer.parseInt(coordinates[0]);
            int column = Integer.parseInt(coordinates[1]);
            dataCenter.getRows().get(row).getSlots().get(column).setAvailable(false);
        }
    }

    private List<Server> readServersConfig(final List<String> inputLines, final int numberOfServers) {
        int serverId = 0;
        int sizeOfConfig = inputLines.size();
        List<String> serverLines = inputLines.subList(sizeOfConfig - numberOfServers, sizeOfConfig);
        ArrayList<Server> servers = new ArrayList<>();

        for (String serverLine : serverLines) {
            List<String> params = Arrays.asList(serverLine.split(SPACE));
            Server server = new Server(serverId++, Integer.parseInt(params.get(1)), Integer.parseInt(params.get(0)));
            servers.add(server);
        }

        return servers;
    }

    private DataCenter createDataCenter(final DataCenterConfig config) {
        DataCenter dataCenter = new DataCenter();
        dataCenter.setConfig(config);
        for (int i = 0; i < config.getNumberOfRows(); i++) {
            Row row = new Row(i + 1);
            for (int j = 0; j < config.getNumberOfSlots(); j++) {
                Slot slot = new Slot();
                row.addSlot(slot);
            }
            dataCenter.addRow(row);
        }

        return dataCenter;
    }

    private DataCenterConfig readConfig(final String configLine) {

        List<String> params = Arrays.asList(configLine.split(SPACE));
        final int rows = Integer.parseInt(params.get(0));
        final int slots = Integer.parseInt(params.get(1));
        final int uSlots = Integer.parseInt(params.get(2));
        final int pools = Integer.parseInt(params.get(3));
        final int servers = Integer.parseInt(params.get(4));
        return new DataCenterConfig(rows, slots, uSlots, pools, servers);
    }

}
