package processor;

import domain.DataCenter;
import domain.Row;
import domain.Server;
import domain.Slot;
import java.util.ArrayList;
import java.util.List;

public class Allocator {

    public void allocate(final DataCenter dataCenter) {
        final float averagePerRowCapacity = dataCenter.calculateAverageCapacityPerRow();
        System.out.println("Avarage capacity per row: " + averagePerRowCapacity);
        System.out.println("Unallocated amount of available slots before processing " + dataCenter.getUnallocatedSlots());
        System.out.println("Unallocated amount of available servers before processing " + dataCenter.getUnallocatedServers());

        for (Row row : dataCenter.getRows()) {
            List<Slot> slots = row.getSlots();
            List<List<Slot>> subSlots = cutToSubSlots(slots);
            int totalRowCapacity = assignServerToSubSlots(subSlots, dataCenter);
            row.setTotalCapacity(totalRowCapacity);

            System.out.println("row id " + row.getRowId() + " capacity before averaging " + totalRowCapacity);
        }

        assignServerToPools(dataCenter);
        //and then assign servers to slots
        //calculate score here
        //output result and generate result file from Main class

        System.out.println("\nUnallocated amount of available slots after processing " + dataCenter.getUnallocatedSlots());
        System.out.println("Unallocated amount of available servers after processing " + dataCenter.getUnallocatedServers());
    }

    private void assignServerToPools(DataCenter dataCenter) {
        final int numberOfPools = dataCenter.getConfig().getNumberOfPools();
        final List<Server> allocatedServers = dataCenter.getAllocatedServers();
        final List<Row> rows = dataCenter.getRows();
        int serversPerPool = allocatedServers.size() / numberOfPools;
        int poolId = 0;

        System.out.println("\nAssigning servers to pools. Pool count is " + numberOfPools);
//        for (Row row : rows) {
//            for (Slot slot : row.getSlots()) {
//                Server server = dataCenter.getServerById(slot.getServerId());
//                server.setPoolId(poolId);
//                poolId++;
//                poolId = poolId % numberOfPools;
//            }
//        }
        for (int i = 0; i < allocatedServers.size(); i++) {
            Server server = allocatedServers.get(i);
            server.setPoolId(poolId);
            if (i != 0 && i % serversPerPool == 0 && poolId < numberOfPools) {
                poolId++;
            }
        }

    }

    private int assignServerToSubSlots(final List<List<Slot>> listOfSubSlots, final DataCenter dataCenter) {
        return listOfSubSlots
            .stream()
            .mapToInt(subSlotList -> allocateSubSlots(dataCenter, subSlotList))
            .sum();
    }

    private int allocateSubSlots(final DataCenter dataCenter, final List<Slot> subSlotList) {
        if (subSlotList.isEmpty()) {
            return 0;
        }

        int capacity = 0;
        final int size = subSlotList.size();
        final List<Integer> allServerSizes = dataCenter.getAllAvailableServerSizes();

        if (allServerSizes.size() == 0) {
            return 0;
        }

        final Integer biggestSeverSize = allServerSizes.get(allServerSizes.size() - 1);

        if (allServerSizes.contains(size)) {
            Server server = dataCenter.getServerOfSizeAndMaxCapacity(size).get();

            subSlotList.forEach(slot -> slot.setServerId(server.getServerId()));
            server.setAllocated(true);
            capacity += server.getCapacity();
        } else if (biggestSeverSize > size) {
            for (int i = size - 1; i > 0; i--) {
                List<Server> servers = dataCenter.getServersOfSize(i);
                if (!servers.isEmpty()) {
                    capacity += allocateSubSlots(dataCenter, subSlotList.subList(0, i));
                    capacity += allocateSubSlots(dataCenter, subSlotList.subList(i, subSlotList.size()));
                }
            }
        } else {
            capacity += allocateSubSlots(dataCenter, subSlotList.subList(0, biggestSeverSize));
            capacity += allocateSubSlots(dataCenter, subSlotList.subList(biggestSeverSize, subSlotList.size()));
        }

        return capacity;
    }

    private List<List<Slot>> cutToSubSlots(final List<Slot> slots) {
        ArrayList<List<Slot>> subLists = new ArrayList<>();
        int start = 0;

        for (int i = 0; i < slots.size(); i++) {
            if (!slots.get(i).isAvailable()) {
                List<Slot> subList = slots.subList(start, i);
                if (!subList.isEmpty()) {
                    subLists.add(subList);
                }
                start = i + 1;
            }
        }
        List<Slot> subList = slots.subList(start, slots.size());
        if (!subList.isEmpty()) {
            subLists.add(subList);
        }
        return subLists;
    }
}
