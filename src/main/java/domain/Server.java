package domain;

public class Server {
    private final int serverId;
    private final int capacity;
    private final int size;
    private int poolId;
    private boolean isAllocated;

    public Server(final int serverId, final int capacity, final int size) {
        this.serverId = serverId;
        this.capacity = capacity;
        this.size = size;
    }

    public int getServerId() {
        return serverId;
    }

    public int getCapacity() {
        return capacity;
    }

    public int getSize() {
        return size;
    }

    public int getPoolId() {
        return poolId;
    }

    public void setPoolId(final int poolId) {
        this.poolId = poolId;
    }

    public boolean isAllocated() {
        return isAllocated;
    }

    public void setAllocated(final boolean allocated) {
        isAllocated = allocated;
    }
}
