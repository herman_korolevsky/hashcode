package domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class DataCenter {

    private final List<Row> rows;
    private List<Server> servers;
    private DataCenterConfig config;

    public DataCenter() {
        this.rows = new ArrayList<>();
    }

    public void addRow(Row row) {
        rows.add(row);
    }

    public List<Row> getRows() {
        return rows;
    }

    public void setServers(final List<Server> servers) {
        this.servers = servers;
    }

    public int calculateTotalCapacityOfAllServers() {
        return servers
            .stream()
            .mapToInt(Server::getCapacity)
            .sum();
    }

    public float calculateAverageCapacityPerRow() {
        return calculateTotalCapacityOfAllServers() / (float) rows.size();
    }

    public long getUnallocatedSlots() {
        return rows
            .stream()
            .map(Row::getSlots)
            .flatMap(Collection::stream)
            .filter(Slot::isAvailable)
            .filter(it -> it.getServerId() == -1)
            .count();
    }

    public long getUnallocatedServers() {
        return servers
            .stream()
            .filter(it -> !it.isAllocated())
            .count();
    }

    public List<Server> getAllocatedServers() {
        return servers
            .stream()
            .filter(Server::isAllocated)
            .collect(Collectors.toList());
    }

    public List<Integer> getAllAvailableServerSizes() {
        return servers
            .stream()
            .filter(it -> !it.isAllocated())
            .map(Server::getSize)
            .distinct()
            .sorted()
            .collect(Collectors.toList());
    }

    public Optional<Server> getServerOfSizeAndMaxCapacity(int size) {
        return getServersOfSize(size)
            .stream()
            .max(Comparator.comparingInt(Server::getCapacity));

    }

    public List<Server> getServersOfSize(int size) {
        return servers
            .stream()
            .filter(it -> it.getSize() == size)
            .filter(it -> !it.isAllocated())
            .collect(Collectors.toList());
    }

    public Optional<Server> getServerOfSize(int size) {
        return servers
            .stream()
            .filter(it -> it.getSize() == size)
            .filter(it -> !it.isAllocated())
            .findFirst();
    }

    public Server getServerById(int id){
        return servers.get(id);
    }

    public DataCenterConfig getConfig() {
        return config;
    }

    public void setConfig(DataCenterConfig config) {
        this.config = config;
    }
}
