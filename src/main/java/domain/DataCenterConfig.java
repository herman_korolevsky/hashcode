package domain;

public class DataCenterConfig {

    private int numberOfRows;
    private int numberOfSlots;
    private int numberOfUnavailableSlots;
    private int numberOfPools;
    private int numberOfServers;

    public DataCenterConfig(final int numberOfRows,
        final int numberOfSlots,
        final int numberOfUnavailableSlots,
        final int numberOfPools,
        final int numberOfServers) {
        this.numberOfRows = numberOfRows;
        this.numberOfSlots = numberOfSlots;
        this.numberOfUnavailableSlots = numberOfUnavailableSlots;
        this.numberOfPools = numberOfPools;
        this.numberOfServers = numberOfServers;
    }

    public int getNumberOfRows() {
        return numberOfRows;
    }

    public int getNumberOfSlots() {
        return numberOfSlots;
    }

    public int getNumberOfUnavailableSlots() {
        return numberOfUnavailableSlots;
    }

    public int getNumberOfPools() {
        return numberOfPools;
    }

    public int getNumberOfServers() {
        return numberOfServers;
    }
}
