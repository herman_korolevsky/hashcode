package domain;

public class Slot {
    private boolean isAvailable;
    private int serverId;

    public Slot() {
        this.isAvailable = true;
        this.serverId = -1;
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    public void setAvailable(boolean available) {
        this.isAvailable = available;
    }

    public int getServerId() {
        return serverId;
    }

    public void setServerId(final int serverId) {
        this.serverId = serverId;
    }
}
