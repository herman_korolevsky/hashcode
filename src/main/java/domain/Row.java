package domain;

import java.util.ArrayList;
import java.util.List;

public class Row {
    private final List<Slot> slots;
    private final int rowId;
    private int totalCapacity;

    public Row(final int rowId) {
        slots = new ArrayList<>();
        this.rowId = rowId;
    }

    public void addSlot(Slot slot) {
        slots.add(slot);
    }

    public List<Slot> getSlots() {
        return slots;
    }

    public int getTotalCapacity() {
        return totalCapacity;
    }

    public void setTotalCapacity(final int totalCapacity) {
        this.totalCapacity = totalCapacity;
    }

    public int getRowId() {
        return rowId;
    }
}
